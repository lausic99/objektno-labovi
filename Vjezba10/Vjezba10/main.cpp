#include<iostream>
#include <fstream>
#include<string>

using namespace std;

class Iznimka{
public:
	string poruka;
	virtual void printajPoruku() = 0;
	virtual string dohvatiPoruku() = 0;
};

class Broj : public Iznimka {
public:
	string poruka = "Broj je krivo unesen!\n";
	void printajPoruku() {
		cout << poruka;
	}
	string dohvatiPoruku() {
		return poruka;
	}
};

class Operatori : public Iznimka {
public:
	string poruka = "Operator nije jedan od podrzanih! (+-*/)!\n";
	void printajPoruku() {
		cout << poruka;
	}
	string dohvatiPoruku() {
		return poruka;
	}
};

class dijeljenjeSNulom : public Iznimka {
public:
	string poruka = "Dijeljenje s nulom nije podrzano!\n";
	void printajPoruku() {
		cout << poruka;
	}
	string dohvatiPoruku() {
		return poruka;
	}
};



int unosBroja() {
	int br;
	cout << "Unesite broj: ";
	cin >> br;
	cout << endl;
	if (cin.fail()) {
		throw Broj();
	}
	return br;
}

char unosOperanda() {
	char ch;
	cout << "Unesite operand: ";
	cin >> ch;
	cout << endl;
	if (ch != '+' && ch != '-' && ch != '*' && ch != '/') {
		throw Operatori();
	}
	return ch;
}

int izracunRezultata(int a, int b, char op) {
	if (op == '+') {
		return a + b;
	}
	else if (op == '-') {
		return a - b;
	}
	else if (op == '*') {
		return a * b;
	}
	else if (op == '/') {
		if (b == 0) {
			throw dijeljenjeSNulom();
		}
		else {
			return a / b;
		}
	}
}

void main() {
	int a, b;
	char op;

	while (true) {
		try {
			a = unosBroja();
			b = unosBroja();
			op = unosOperanda();
			cout << izracunRezultata(a, b, op) << endl;
		}
		catch (Iznimka & x) {
			ofstream fout("errors.log", ios_base::out | ios_base::app);
			fout << x.dohvatiPoruku() << endl;
			if (!x.dohvatiPoruku().empty()) {
				break;
			}
			//x.printajPoruku();
		}
	}

}