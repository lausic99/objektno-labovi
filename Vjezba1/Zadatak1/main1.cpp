﻿/*U nizu od N cijelih brojeva, nalaze se duplikati.
Pronadi ih i ispiˇsi koliko puta se ponavljaju! Mogu´ce su samo vrijednosti 1-9
*/

#include <iostream>
#include <algorithm>
#include <array>

int main() {

	int niz[] = { 5, 4, 9, 4, 1, 8,8,8,8,8,3, 7, 1, 1, 9 };
	int len = std::size(niz);
	std::sort(niz, niz + len);

	int tr = niz[0], kol = 1;
	for (int i = 1; i <= len; i++) {
		if (niz[i] == tr) {
			kol++;
		}
		else {
			if (kol > 1) {
				std::cout << "Broj " << tr << " se ponavlja " << kol << " puta." << std::endl;
			}
			tr = niz[i];
			kol = 1;
		}
	}
}
