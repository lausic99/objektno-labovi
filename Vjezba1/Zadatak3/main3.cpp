/* U nizu �cetveroznamenkastih brojeva nalazi se samo jedan broj �cija je suma jedinice i stotice jednaka 5.
Napi�site funkciju koja pronalazi taj broj u nizu od N brojeva i vra�ca referencu na taj element niza.
Koriste�ci povratnu vrijednost funkcije kao lvalue uve�cajte taj element niza za jedan.
Ispi�site vrijednost elementa poslije uve�cavanja. */


#include <iostream>
#include <array>

int* trazi(int niz[], int len) {

	for (int i = 0; i < len; i++) {
		if ((niz[i] % 10 + (niz[i] / 100) % 10) == 5) {
			return &niz[i];
		}
	}
	return NULL;
}

int main() {
	int niz[] = { 1234,3241,8527,9999,4357,3124,4455,1743,6615,4566 };
	int len = std::size(niz);
	int* taj = trazi(niz, len);
	//std::cout << *taj;
	*taj += 1;
	//std::cout << *taj;
	for (int i = 0; i < len; i++) {
		std::cout << niz[i] << " ";
	}
}