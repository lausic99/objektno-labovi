﻿/*Napiˇsite program za vodenje evidencije i statistiˇcku analizu za razred od 20 studenata.
Informacije o svakom studentu sadrˇze ID string, ime, spol, ocjene kvizova (2 kviza po semestru),
ocjenu na sredini semestra (mid-term score), ocjenu na kraju semestra (ﬁnal score)
i ukupan broj bodova koji je zbroj kviza1, kviza2, ocjene na sredini i ocjene na kraju semestra (total score).
Kreiraj strukturu student i napiˇsi implementaciju za izbornik ispod
• Dodaj novi zapis +
• Ukloni zapis+
• Aˇzuriraj zapis +
• Prikaˇzi sve zapise +
• Izraˇcunaj prosjek bodova za studenta +
• Prikaˇzi studenta s najve´cim brojem bodova +
• Prikaˇzi studenta s najmanjim brojem bodova +
• Pronadi studenta po ID-u +
• Sortiraj zapise po broju bodova (total) +
• Izlaz+
Imati na umu da se korisniku ne smije dozvoliti unos za studenta s ve´c postoje´cim IDijem,
te sluˇcajeve kad u nizu nema zapisa, kad korisnik upiˇse ID koji ne postoji u nizu i sliˇcno.*/

#include <iostream>
#include <string>
#include<algorithm>
using namespace std;

typedef struct Stu {
	char ID[5];
	std::string Ime;
	char Spol[10];
	int kviz[2];
	int mid_term_score;
	int final_score;
	int total_score;
}Student;

//Prikaˇzi studenta s najmanjim brojem bodova 
void pronadi_manji(Student* razred) {
	int i = 0;
	for (int v = 0; v < 20 && razred[v].ID[0] != '\0'; v++) {
		if (razred[v].total_score < razred[i].total_score) {
			i = v;
		}
	}
	cout << "ID studenta: " << razred[i].ID << endl;
	cout << "Ime i prezime studenta: " << razred[i].Ime << endl;
	cout << "Spol studenta: " << razred[i].Spol << endl;
	cout << "Ocjena iz prvog kviza: " << razred[i].kviz[0] << ", ocjena iz drugog kviza: " << razred[i].kviz[1] << endl;
	cout << "Ocjena na sredini semestra: " << razred[i].mid_term_score << endl;
	cout << "Ocjena na kraju semestra: " << razred[i].final_score << endl;
	cout << "Ukupan broj bodova: " << razred[i].total_score << endl;
	cout << endl;
}


//Prikaˇzi studenta s najve´cim brojem bodova 
void pronadi_najveci(Student* razred) {
	int i = 0;
	for (int v = 0; v < 20; v++) {
		if (razred[v].total_score > razred[i].total_score) {
			i = v;
		}
	}
	cout << "ID studenta: " << razred[i].ID << endl;
	cout << "Ime i prezime studenta: " << razred[i].Ime << endl;
	cout << "Spol studenta: " << razred[i].Spol << endl;
	cout << "Ocjena iz prvog kviza: " << razred[i].kviz[0] << ", ocjena iz drugog kviza: " << razred[i].kviz[1] << endl;
	cout << "Ocjena na sredini semestra: " << razred[i].mid_term_score << endl;
	cout << "Ocjena na kraju semestra: " << razred[i].final_score << endl;
	cout << "Ukupan broj bodova: " << razred[i].total_score << endl;
	cout << endl;
}

//Pronadi studenta po ID-u 
void pronadi(Student* razred) {
	cout << "Unesite ID studenta: ";
	char trazeni[5];
	cin >> trazeni;
	cout << endl;
	int i;
	for (i = 0; i < 20; i++) {
		if (!strcmp(trazeni, razred[i].ID)) {
			cout << "ID studenta: " << razred[i].ID << endl;
			cout << "Ime i prezime studenta: " << razred[i].Ime << endl;
			cout << "Spol studenta: " << razred[i].Spol << endl;
			cout << "Ocjena iz prvog kviza: " << razred[i].kviz[0] << ", ocjena iz drugog kviza: " << razred[i].kviz[1] << endl;
			cout << "Ocjena na sredini semestra: " << razred[i].mid_term_score << endl;
			cout << "Ocjena na kraju semestra: " << razred[i].final_score << endl;
			cout << "Ukupan broj bodova: " << razred[i].total_score << endl;
			cout << endl;
			break;
		}
	}
	if (i == 20) {
		cout << "Student s tim ID-om ne postoji" << endl;
	}
}
//Sortiraj zapise po broju bodova (total) 
void sortiraj(Student*Razred) {
	for (int i = 0; i < 20; i++) {
		for (int j = i + 1; j < 20; j++) {
			if (Razred[i].ID[0] != '\0') {
				if (Razred[i].total_score < Razred[j].total_score) {
					Student tmp = Razred[j];
					Razred[j] = Razred[i];
					Razred[i] = tmp;
				}
			}
		}
	}
}


//Prikaˇzi sve zapise-gotov
void prikazi(Student*razred) {
	for (int i = 0; i < 20 && (razred[i].ID[0] != '\0'); i++) {
		cout << "ID studenta: " << razred[i].ID << endl;
		cout << "Ime i prezime studenta: " << razred[i].Ime << endl;
		cout << "Spol studenta: " << razred[i].Spol << endl;
		cout << "Ocjena iz prvog kviza: " << razred[i].kviz[0] << ", ocjena iz drugog kviza: " << razred[i].kviz[1] << endl;
		cout << "Ocjena na sredini semestra: " << razred[i].mid_term_score << endl;
		cout << "Ocjena na kraju semestra: " << razred[i].final_score << endl;
		cout << "Ukupan broj bodova: " << razred[i].total_score << endl;
		cout << endl;
	}

}

//dodaj novi zapis-gotov
void dodaj(Student*razred) {
	cout << "Unesite ID studenta: ";
	char tmp[5];
	cin >> tmp;
	cout << endl;
	for (int i = 0; i < 20;) {
		if (!strcmp(tmp, razred[i].ID)) {
			cout << "ID zauzet, unesite drugi: ";
			cin >> tmp;
			cout << endl;
			i = 0;
		}
		else {
			i++;
		}
	}
	int j;
	for (j = 0; j < 20;) {
		if (razred[j].ID[0] == '\0') {
			break;
		}
		else {
			j++;
		}
	}



	strcpy_s(razred[j].ID, tmp);
	cin.ignore();//bolje
	cout << "Unesite ime i prezime studenta: ";
	getline(cin, razred[j].Ime);
	cout << endl;

	cout << "Unesite spol studenta: ";
	cin >> razred[j].Spol;
	cout << endl;

	cout << "Unesite ocjene kvizova studenta : ";
	cin >> razred[j].kviz[0] >> razred[j].kviz[1];
	cout << endl;


	cout << "Unesite ocjenu studenta na sredini semestra: ";
	cin >> razred[j].mid_term_score;
	cout << endl;


	cout << "Unesite ocjenu studenta na kraju semestra: ";
	cin >> razred[j].final_score;
	cout << endl;

	razred[j].total_score = razred[j].kviz[0] + razred[j].kviz[1] + razred[j].mid_term_score + razred[j].final_score;
	//cout << razred[razred->kol].total_score << endl;
}

//Ukloni zapis-gotov
void ukloni(Student*razred) {
	cout << "Unesite ID studenta: ";
	char tmp[5];
	cin >> tmp;
	cout << endl;
	int koji = -1;
	for (int i = 0; i <= 20;) {
		if (i == 20 && koji == -1) {
			cout << "Student s tim ID-om ne postoji, unesite drugi: ";
			cin >> tmp;
			cout << endl;
			i = 0;
		}

		else {

			if (!strcmp(tmp, razred[i].ID)) {
				koji = i;
				//cout << "postoji";
			}
			i++;
		}
	}
	razred[koji].total_score = 0;
	razred[koji].final_score = 0;
	razred[koji].mid_term_score = 0;
	razred[koji].kviz[0] = 0;
	razred[koji].kviz[0] = 0;
	razred[koji].Spol[0] = '\0';
	razred[koji].Ime[0] = '\0';
	razred[koji].ID[0] = '\0';
}

//• Aˇzuriraj zapis -gotov
void azuriraj(Student*razred) {
	cout << "Unesite ID studenta: ";
	char tmp[5];
	cin >> tmp;
	cout << endl;
	int koji = -1;
	for (int i = 0; i <= 20;) {
		if (i == 20 && koji == -1) {
			cout << "Student s tim ID-om ne postoji, unesite drugi: ";
			cin >> tmp;
			cout << endl;
			i = 0;
		}

		else {

			if (!strcmp(tmp, razred[i].ID)) {
				koji = i;

			}
			i++;
		}
	}
	int odabir = -1;

	while (odabir != 0) {
		cout << "Odaberite sta azurirati: ";
		cout << "1 - Ime i prezime" << endl;
		cout << "                         2 - Spol" << endl;
		cout << "                         3 - Ocjena prvog kviza" << endl;
		cout << "                         4 - Ocjena drugog kviza" << endl;
		cout << "                         5 - Ocjena na sredini semestra" << endl;
		cout << "                         6 - Ocjena na kraju semestra" << endl;
		cout << "                         0 - IZLAZ" << endl;


		cin >> odabir;

		switch (odabir) {
		case 1:
			cout << "Azurirajte ime i prezime: ";
			cin.ignore();
			getline(cin, razred[koji].Ime);
			break;
		case 2:
			cout << "Azurirajte spol: ";
			cin >> razred[koji].Spol;
			break;
		case 3:
			cout << "Azurirajte ocjenu prvog kviza: ";
			cin >> razred[koji].kviz[0];
			break;
		case 4:
			cout << "Azurirajte ocjenu drugog kviza: ";
			cin >> razred[koji].kviz[1];
			break;
		case 5:
			cout << "Azurirajte ocjenu na sredini semestra: ";
			cin >> razred[koji].mid_term_score;
			break;
		case 6:
			cout << "Azurirajte ocjenu na kraju semestra: ";
			cin >> razred[koji].final_score;
			break;
		default:
			break;
		}
	}
	razred[koji].total_score = razred[koji].kviz[0] + razred[koji].kviz[1] + razred[koji].mid_term_score + razred[koji].final_score;
}

//Izraˇcunaj prosjek bodova za studenta -gotov
void prosjek(Student*razred) {
	cout << "Unesite ID studenta: ";
	char tmp[5];
	cin >> tmp;
	cout << endl;
	int koji = -1;
	for (int i = 0; i <= 20;) {
		if (i == 20 && koji == -1) {
			cout << "Student s tim ID-om ne postoji, unesite drugi: ";
			cin >> tmp;
			cout << endl;
			i = 0;
		}

		else {

			if (!strcmp(tmp, razred[i].ID)) {
				koji = i;

			}
			i++;
		}
	}
	cout << "ID: " << razred[koji].ID << " student: ";
	cout << razred[koji].Ime << " ima prosjecnu ocjenu: " << (razred[koji].total_score / 4) << endl;
}


int main () {
	Student razred[20];
	for (int i = 0; i < 20; i++) {
		razred[i].ID[0] = '\0';
	}

	/*for (int i = 0; i < 3;  i++) {
		dodaj(razred);
	}
	//prikazi(razred);
	//ukloni(razred);
	//azuriraj(razred);
	//prikazi(razred);
	//prosjek(razred);
	//sortiraj(razred);
	//prikazi(razred);
	//pronadi(razred);
	//pronadi_najveci(razred);
	//pronadi_manji(razred);
	*/
	int odabir = -1;
	while (odabir != 0) {
		cout << "Odaberite: ";
		cout << "1 - Dodaj zapis" << endl;
		cout << "           2 - Ukloni zapis" << endl;
		cout << "           3 - Azuriraj zapis" << endl;
		cout << "           4 - Prikazi sve zapise" << endl;
		cout << "           5 - Izracunaj prosjek bodova za studenta" << endl;
		cout << "           6 - Prikazi studenta s najvecim brojem bodova" << endl;
		cout << "           7 - Prikazi studenta s najmanjim brojem bodova" << endl;
		cout << "           8 - Pronadi studenta po ID-u" << endl;
		cout << "           9 - Sortiraj zapise po broju bodova(total)" << endl;
		cout << "           0 - IZLAZ" << endl;

		cin >> odabir;

		switch (odabir) {
		case 1:
			dodaj(razred);
			break;
		case 2:
			ukloni(razred);
			break;
		case 3:
			azuriraj(razred);
			break;
		case 4:
			prikazi(razred);
			break;
		case 5:
			prosjek(razred);
			break;
		case 6:
			pronadi_najveci(razred);
			break;
		case 7:
			pronadi_manji(razred);
			break;
		case 8:
			pronadi(razred);
			break;
		case 9:
			sortiraj(razred);
			break;
		default:
			break;
		}
	}

}
