#include<iostream>

using namespace std;

void funk(int*niz, int& len, int k) {

	for (int i = 0; i < len; i++) {
		if (niz[i] % k == 0) {

			for (int j = i; j < len-1; j++) {
				niz[i] = niz[j + 1];
			}
			len--;
		}
	}
}

int main() {
	cout << "Unesite duljinu niza: "<<endl;
	int len;
	cin >> len;
	cout << "Unesite broj k: " << endl;
	int k;
	cin >> k;
	int* niz = new int[len];
	/*niz[0] = 2;
	niz[1] = 5;
	niz[2] = 8;
	niz[3] = 55;
	niz[4] = 9;
	niz[5] = 3;*/
	for (int i = 0; i < len; i++) {
		cin >> niz[i];
	}
	funk(niz, len, k);
	for (int i = 0; i < len; i++) {
		cout << niz[i]<<", ";
	}
}
