#include "stack.h"
#include<algorithm>

stack::stack(int size) {
	niz = new int[size];
	this->size = size;
	this->koji = size;
}

stack::stack(const stack&other) {
	this->size = other.size;
	this->koji = other.koji;
	niz = new int[size];
	for (int i = 0; i < this->size; i++) {
		niz[i] = other.niz[i];
	}
}

stack::~stack() {
	delete[] niz;
}

stack stack::stacke() {
	this->niz = new int[0];
	return *this;
}

bool stack::empty() {
	if (this->size == 0) {
		return 1;
	}
	return 0;
}


int stack::top() {
	return this->niz[this->koji];
}

int stack::pop() {
	if (this->koji < this->size) {
		this->koji += 1;
	}
	return this->niz[this->koji -1];
}

void stack::pushk(int element) {
	this->niz[this->koji-1] = element;
	this->koji -= 1;
}

stack& stack::operator=(const stack& other) {
	this->niz = other.niz;
	this->size = other.size;
	this->koji = other.koji;
	for(int i = 0; i < this->size; i++) {
		niz[i] = other.niz[i];
	}
	return *this;
}

stack operator+(const stack& a, const stack& b) {
	stack novi(a.size + b.size);
	int i,j;
	for (i = 0; i < a.size; i++) {
		novi.niz[i] = a.niz[i];
	}
	for (j = 0; j < b.size; j++,i++) {
		novi.niz[i] = b.niz[j];
	}
	novi.koji -= i;
	return novi;
}

int compareints(const void* a, const void * b) {
	return (*(int*)a - *(int*)b);
}


bool stack::operator==(const stack& b) {
	if (this->size != b.size) {
		return 0;
	}
	qsort(this->niz, this->size, sizeof(int), compareints);
	qsort(b.niz, b.size, sizeof(int), compareints);
	for (int i = 0; i < this->size; i++) {
		if (this->niz[i] != b.niz[i]) {
			return 0;
		}
	}
	return 1;
}

ostream& operator<<(ostream& out, const stack& a) {
	for (int i = 0; i < a.size; i++) {
		out << a.niz[i] << "  " << endl;
	}
	return out;
}
