#ifndef STACK_H
#define STACK_H

#include<istream>
#include<ostream>
using namespace std;

class stack {
	int*niz;
	int size;
	int koji;


public:
	stack();
	stack(int size);
	stack(const stack&other);
	~stack();

	stack stacke();
	bool empty();
	int top();
	int pop();
	void pushk(int element);
	stack& operator=(const stack& other);

	friend stack operator+(const stack& a, const stack& b);
	bool operator==(const stack& a);
	friend ostream& operator <<(ostream& out, const stack& a);

};


#endif // !STACK_H

