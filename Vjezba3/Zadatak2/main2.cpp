/*Koriste�ci funkcije iz prvog zadatka unijeti dva vektora i
formirati novi vektor koji se sastoji od elemenata iz prvog vektora
koji nisu u drugom vektoru. Koristiti binary search funkciju.
Funkcije i njihove prototipe napisati u posebnim datotekama.*/

#include"vectori.h"
#include <iostream>
#include <time.h>
#include <algorithm>
using namespace std;
vector<int> zanovi(vector<int> niz1, vector<int> niz2) {
	vector<int> novi;
	sort(niz1.begin(), niz1.end());
	sort(niz2.begin(), niz2.end());

	for (int i = 0; i < niz1.size(); i++) {
		if (!binary_search(niz2.begin(), niz2.end(), niz1.at(i))) {
			novi.push_back(niz1.at(i));
		}
	}
	return novi;
}

int main() {
	srand(time(0));
	vector<int> niz1 = unos(1, 0, 100, 4);
	vector<int> niz2 = unos(1, 0, 100, 4);
	print(niz1);
	print(niz2);

	vector<int> novi = zanovi(niz1, niz2);
	/*int i;
	int j;
	for (i = 0; i < niz1.size(); i++) {
		for (j = 0; j < niz2.size(); j++) {
			if (niz1.at(i) == niz2.at(j)) {
				break;
			}

		}
		if (j == niz2.size()) {
			novi.push_back(niz1.at(i));
		}
	}
	*/
	print(novi);
}