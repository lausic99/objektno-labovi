#include"vectori.h"
vector<int> unos(bool dali, int a, int b, int n) {

	vector<int> niz;
	int x;

	if (dali) {
		for (int i = 0; i < n; i++) {
			cout << "Unesite broj na poziciji " << i << endl;
			cin >> x;
			if (x > b || x < a) {
				cout << "Broj nije u granicama, unesite ponovo!" << endl;
				i--;
			}
			else {
				niz.push_back(x);
			}
		}
	}
	else {
		for (int i = 0; i < n; i++) {
			x = (rand() % (b - a)) + a;
			niz.push_back(x);
		}
	}
	return niz;
}

void print(vector<int> niz) {
	for (int i = 0; i < niz.size(); i++) {
		cout << niz.at(i) << " ";
	}
	cout << endl;
}