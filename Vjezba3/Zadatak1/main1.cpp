/*Napisati funkcije za unos i ispis vektora.
Funkcija za unos treba imati sljede�ce parametre: a i b koji
predstavljaju granice intervala iz kojeg moraju biti elementi vektora
i ?cija je pretpostavljena vrijednost 0 i 100, broj elemenata n sa
pretpostavljenom vrijedno?s�cu 5 i bool vrijednost koja odreduje
ho�ce li se elementi upisivati ili �ce se generirati
pseudorandom funkcijom. Funkcija za unos vra�ca vektor.*/

#include <iostream>
#include<vector>
#include<time.h>

using namespace std;

vector<int> unos(bool dali, int a = 0, int b = 100, int n = 5) {

	vector<int> niz;
	int x;

	if (dali) {
		for (int i = 0; i < n; i++) {
			cout << "Unesite broj na poziciji " << i << endl;
			cin >> x;
			if (x > b || x < a) {
				cout << "Broj nije u granicama, unesite ponovo!" << endl;
				i--;
			}
			else {
				niz.push_back(x);
			}
		}
	}
	else {
		for (int i = 0; i < n; i++) {
			x = (rand() % (b - a)) + a;
			niz.push_back(x);
		}
	}
	return niz;
}

void print(vector<int> niz) {
	for (int i = 0; i < niz.size(); i++) {
		cout << niz.at(i) << endl;
	}
}

int main() {
	vector<int> niz;
	srand(NULL);

	bool dali;
	cout << "Da li zelite sami unijeti brojeve u niz? 1-DA 0-NE" << endl;
	cin >> dali;

	if (dali) {
		int od, d, n;
		cout << "Unesite velicinu niza:";
		cin >> n;
		cout << endl;
		cout << "Unesite parametre: " << endl;
		cin >> od >> d;
		niz = unos(dali, od, d, n);
	}
	else {
		niz = unos(dali);
	}

	print(niz);
}