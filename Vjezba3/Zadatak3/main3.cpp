﻿/*Uˇcitati string koji predstavlja reˇcenicu.
Napisati funkciju koja iz stringa izbacuje sve
praznine koje se nalaze ispred znakova interpunkcije i
dodaje praznine nakon znaka interpunkcije ako nedostaju.
Primjer: Za reˇcenicu "Ja bih ,ako ikako mogu , ovu reˇcenicu napisala ispravno .",
ispravna reˇcenica glasi: "Ja bih, ako ikako mogu, ovu reˇcenicu napisala ispravno.".*/

#include <iostream>
#include <string>
using namespace std;
void izbaci(string& str) {

	for (int i = 0; i < str.length(); i++) {
		if (str[i] == ' ' && (str[i + 1] == ',' || str[i + 1] == '.' || str[i + 1] == ' ')) {
			str.replace(str.begin() + i, str.begin() + i + 1, "");
			i--;
		}

	}
	for (int i = 0; i < str.length(); i++) {
		if (str[i] == ',' && str[i + 1] != ' ') {
			//str.replace(str.begin() + i + 1, str.begin() + i + 2, " ");
			str.insert(i + 1, " ");
		}
	}

}
int main() {
	//string str = "Ja bih    ,ako ikako mogu    ,     ovu recenicu napisala ispravno .";
	string str;
	cout << "Unesite string: ";
	getline(cin, str);
	cout << endl;
	cout << str << endl;
	izbaci(str);
	cout << str << endl;
	cout << str.length();
}