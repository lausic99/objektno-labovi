﻿/*Korisnik unosi prirodni broj N, nakon ˇcega unosi N stringova u vector
(duljina svakog stringa mora biti manja od 20 - provjeriti unos,
dozvoljeni su samo brojevi i velika slova engleske abecede). Ukoliko string sadrˇzava slova i brojeve,
onda ga treba trasnformirati na naˇcin da se svaki podstring od 2 ili viˇse istih znakova zamijeni
brojem ponavljanja (AAA = 3A). S druge strane, ukoliko string sadrˇzava samo slova onda ga treba
transformirati suprotno prethodno opisanoj transformaciji (2F = FF).
npr. N=5,
Unos: AABFBBBBCC, AABFBBBCCC AAAAFBFFFBBBCCC, 2F8H4SF
Ispis: 2ABF4B2C, 2ABF3B3C, 4AFB3F3B3C, FFHHHHHHHHSSSSF
*/

#include <iostream>
#include <vector>
#include<string>
using namespace std;

vector<string> razvrstaj(vector<string> vek, int n) {


	for (int i = 0; i < vek.size(); i++) {
		string tmp;
		for (int j = 0; j < vek[i].length(); j++) {

			if (isdigit(vek[i].at(j))) {

				int kol = 0;
				for (int l = 0; l < vek[i].length(); l++) {

					if (isdigit(vek[i].at(l))) {
						kol = kol * 10;
						kol += vek[i].at(l) - '0';
					}
					else {
						tmp.push_back(vek[i].at(l));
						if (kol > 0) {
							while (kol > 1) {
								tmp.push_back(vek[i].at(l));
								kol--;
							}
							kol = 0;
						}
					}
				}
				tmp.push_back('\0');
				//cout << tmp<<"      ";
				vek[i] = tmp;
				//cout << vek[i];
				break;
			}

			else if (j == vek[i].length() - 1) {

				char c = vek[i].at(0);
				int kol = 1;
				for (int l = 1; l <= vek[i].length(); l++) {
					if (l < vek[i].length()) {
						if (vek[i].at(l) == c) {
							kol += 1;
						}
						else {
							if (kol > 1) {
								tmp.push_back('0' + kol);
								tmp.push_back(c);
							}
							else {
								tmp.push_back(c);
							}
							c = vek[i].at(l);
							kol = 1;
						}
					}
					else {
						if (kol > 1) {
							tmp.push_back('0' + kol);
							tmp.push_back(c);
						}
						else {
							tmp.push_back(c);
						}
					}
				}
				vek[i] = tmp;
				break;
			}
		}
	}
	return vek;
}
int main() {
	cout << "Unesite prirodan broj: ";
	int n;
	cin >> n;

	vector<string> vek(n);
	cin.ignore();
	for (int i = 0; i < n; i++) {
		cout << "Moguce unijeti samo slova eng abecede i brojeve" << endl;
		cin >> vek[i];
		if (vek[i].length() > 20) {
			cout << "Premasena duljina stringa.(MAX20)Unesite ponovno!" << endl;
			i--;
		}
	}
	for (int i = 0; i < vek.size(); i++) {
		cout << i << "  " << vek[i] << endl;
	}

	vek = razvrstaj(vek, n);

	for (int i = 0; i < vek.size(); i++) {
		cout << i << "  " << vek[i] << endl;
	}

}