#include<iostream>
#include <fstream>
#include<string>

using namespace std;

class Iznimka {
public:
	string poruka;
	virtual void prfloatajPoruku() = 0;
	virtual string dohvatiPoruku() = 0;
};

class Broj : public Iznimka {
public:
	string poruka = "Broj je krivo unesen!\n";
	void prfloatajPoruku() {
		cout << poruka;
	}
	string dohvatiPoruku() {
		return poruka;
	}
};

class Operatori : public Iznimka {
public:
	string poruka = "Operator nije jedan od podrzanih! (+-*/)!\n";
	void prfloatajPoruku() {
		cout << poruka;
	}
	string dohvatiPoruku() {
		return poruka;
	}
};

class dijeljenjeSNulom : public Iznimka {
public:
	string poruka = "Dijeljenje s nulom nije podrzano!\n";
	void prfloatajPoruku() {
		cout << poruka;
	}
	string dohvatiPoruku() {
		return poruka;
	}
};

float unosBroja() {
	float br;
	cout << "Unesite broj: ";
	cin >> br;
	cout << endl;
	if (cin.fail()) {
		cin.clear();
		cin.ignore();
		throw Broj();
	}
	return br;
}

char unosOperanda() {
	char ch;
	cout << "Unesite operand: ";
	cin >> ch;
	cout << endl;
	if (ch != '+' && ch != '-' && ch != '*' && ch != '/') {
		throw Operatori();
	}
	return ch;
}

float izracunRezultata(float a, float b, char op) {
	if (op == '+') {
		return a + b;
	}
	else if (op == '-') {
		return a - b;
	}
	else if (op == '*') {
		return a * b;
	}
	else if (op == '/') {
		if (b == 0) {
			throw dijeljenjeSNulom();
		}
		else {
			return a / b;
		}
	}
}

void main() {
	float a, b;
	char op;

	while (true) {
		try {
			a = unosBroja();
			b = unosBroja();
			op = unosOperanda();
			cout << izracunRezultata(a, b, op) << endl;
		}
		catch (Iznimka& x) {
			ofstream fout("errors.log", ios_base::out | ios_base::app);
			fout << x.dohvatiPoruku() << endl;
			if (!x.dohvatiPoruku().empty()) {
				break;
			}
			//x.prfloatajPoruku();
		}
	}
}