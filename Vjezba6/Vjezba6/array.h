﻿/*
1.Kreirajte klasu Array u kojoj se definira niz cijelih brojeva, ˇcija veliˇcina se odreduje u
konstruktoru. Niz je alociran na heapu. Napiˇsite potrebne konstruktore i destruktore.
Preopteretite sljede´ce operatore:
• operatore unosa i ispisa, +
• operator =,  +
• operator +, koji spaja dva niza,  +
• operator -, ˇciji je rezultat novi niz u kojem su elementi prvog niza koji nisu u
drugom nizu,  +
• operatore jednakosti i nejednakosti (ˇclan po ˇclan),++
• operator [], kojem se pristupa ˇclanovima niza po indexu (mogu´ca je i promjena
vrijednosti ˇclana).  +
2. Napiˇsite privatni statiˇcki ˇclan counter koji ´ce ˇcuvati podatak o broju instanciranih
objekata tipa Array i funkciju ˇclan koja ´ce dohva´cati vrijednosti countera.++
3. Klasu iz prvog zadatka stavite u namespace OOP.++
Napomena 1: U funkciji main testirajte svu funkcionalnost Arraya.
Napomena 2: u copy konstruktoru se, osim same vrijednosti pointera, kopiraju i vrijednosti niza (deep copy).
Napomena 3: odvojite implementaciju i suˇcelje klase u razliˇcite datoteke.*/

#ifndef ARAY_H
#define ARAY_H
#include<istream>
#include<ostream>
using namespace std;
namespace OOP {
	class Array {
		int* niz;
		int duljina;
		static int counter;
	public:
		Array();
		Array(int kol);
		Array(const Array& other);
		~Array();

		friend ostream& operator <<(ostream& out, Array&a);
		friend istream& operator >>(istream& in, Array&a);
		friend Array operator +(const Array & a, const Array & b);
		friend Array operator -(const Array & a, const Array & b);
		Array& operator =(const Array & other);
		int& operator [](const int x);
		bool operator ==(const Array& other);
		bool operator !=(const Array& other);

		int get_duljina()const;
		static int get_counter();

	};
}

#endif 

