#include<iostream>
#include "array.h"
#include <algorithm>
using namespace std;


int OOP::Array::counter = 0;

int compareints(const void* a, const void * b) {
	return (*(int*)a - *(int*)b);
}

int OOP::Array::get_duljina() const{
	return this->duljina;
}

int OOP::Array::get_counter(){
	return counter;
}

OOP::Array::Array() {
	++counter;
}

OOP::Array::Array(int kol) {
	++counter;
	niz = new int[kol];
	this->duljina = kol;
}

OOP::Array::Array(const Array& other) {
	niz =new int[other.duljina];
	for (int i = 0; i < other.duljina; i++) {
		niz[i] = other.niz[i];
	}
	duljina = other.duljina;
}


OOP::Array::~Array() {
	delete[] niz;
	//cout << "pozivanje destruktora" << counter << endl;
	counter--;

}

ostream& OOP::operator <<(ostream& os, OOP::Array& a) {
	for (int i = 0; i < a.get_duljina(); i++) {
		//cout << a.get_duljina();
		os << a.niz[i] << " " << endl;
	}
	return os;
}
istream& OOP::operator >>(istream& is, OOP::Array&a) {
	for (int i = 0; i < a.get_duljina(); i++) {
		cout << "Unesite:";
		is >> a.niz[i];
	}
	return is;
}


OOP::Array OOP::operator +(const OOP::Array & a, const OOP::Array & b) {
	OOP::Array novi(a.duljina + b.duljina);
	int i;
	for (i = 0; i < a.duljina; i++) {
		novi.niz[i] = a.niz[i];
	}
	int j;
	for (i, j = 0; i < a.duljina + b.duljina; i++, j++) {
		novi.niz[i] = b.niz[j];
	}
	//cout << novi;
	return novi;
}

OOP::Array OOP::operator -(const OOP::Array & a, const OOP::Array & b) {
	OOP::Array novi(a.duljina + b.duljina);
	sort(a.niz, a.niz + a.duljina);
	sort(b.niz, b.niz + b.duljina);
	int j = 0;
	for (int i = 0; i < b.duljina; i++) {
		if (!bsearch(&a.niz[i], b.niz, b.duljina, sizeof(int), compareints)){
			novi.niz[j] = a.niz[i];
			j++;
		}
	}
	novi.duljina = j;
	return novi;
}


OOP::Array&  OOP::Array::operator =(const OOP::Array & other) {
	niz = other.niz;
	return *this;
}

int& OOP::Array::operator [](const int index) {
	if (index >= duljina){
		cout << "Array index out of bound, exiting";
		exit(0);
	}
	return niz[index];
}

bool OOP::Array::operator ==(const OOP::Array& other) {
	int t = 0, p = 0;
	for (int i = 0; i < this->duljina; i++) {
		for (int j = 0; j < other.duljina; j++) {
			if (niz[i] == other.niz[j]) {
				t += 1;
			}
			if (niz[i] != other.niz[j]) {
				p = 1;
			}
		}
	}
	if (t == duljina && p == 1) {
		return true;
	}
	return false;
}



bool OOP::Array::operator !=(const OOP::Array& other) {
	//cout << "uslo je";
	int t = 0,p=0;
	for (int i = 0; i < this->duljina; i++) {
		for (int j = 0; j < other.duljina; j++) {
			if (niz[i] == other.niz[j]) {
				t += 1;
			}
			if (niz[i] != other.niz[j]) {
				p = 1;
			}
		}
	}
	if (t!=duljina && p==1) {
		return true;
	}
	return false;
}

