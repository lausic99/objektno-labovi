#ifndef FERRY_H
#define FERRY_H

#include "watercraft.h"
class ferry : public watercraft {
protected:
	unsigned totalNumOfAllTogether;
public:
	ferry(unsigned  passengers, unsigned bikes, unsigned cars);
	unsigned passengers();

};

#endif
