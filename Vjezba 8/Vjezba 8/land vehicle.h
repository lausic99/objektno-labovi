#ifndef  LANDVEHICLE_H
#define LANDVEHICLE_H
#include"vehicle.h"

class landVehicle : virtual public vehicle {

protected:
	string vehicleType;
	unsigned numOfPassengers;
public:
	landVehicle() {};
	string type();

};

#endif
