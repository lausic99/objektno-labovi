#ifndef SEAPLANE_H
#define SEAPLANE_H

#include"watercraft.h"
#include"aircraft.h"

class seaplane : public aircraft, public watercraft {
protected:
	unsigned numOfPassengers;
public:
	seaplane(unsigned broj);
	unsigned passengers();
	string type()=0;
};

#endif