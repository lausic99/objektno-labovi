#ifndef  AIRCRAFT_H
#define AIRCRAFT_H
#include"vehicle.h"

class aircraft : virtual public vehicle {

protected:
	string vehicleType;
	unsigned numOfPassengers;
public:
	aircraft() {};
	string type();

};

#endif