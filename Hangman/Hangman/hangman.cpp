#include"hangman.h"

hangman::hangman() {
	this->numOfGuesses = 9;
	this->numOfFails = 0;
	this->randomNum = 0;
	this->wordToGuess = this->loadWord();
	this->hint = this->loadHint();
	this->setGuessedWord();
}

string hangman::loadWord() {

	vector<string> v;
	string wordFromFile;
	ifstream myfile("words.txt");
	if (myfile.is_open()) {

		while (getline(myfile, wordFromFile)) {
			v.push_back(wordFromFile);
		}
		myfile.close();
		this->randomNum = rand() % v.size();
		wordFromFile = v.at(this->randomNum);
	}
	transform(wordFromFile.begin(), wordFromFile.end(), wordFromFile.begin(), ::toupper);
	return wordFromFile;

}

string hangman::loadHint() {
	vector<string> v;
	string hintFromFile;
	ifstream myfile("hints.txt");
	if (myfile.is_open()) {

		while (getline(myfile, hintFromFile)) {
			v.push_back(hintFromFile);
		}
		myfile.close();
		hintFromFile = v.at(this->randomNum);
	}

	return hintFromFile;
}

string hangman::getWord() {
	return this->wordToGuess;
}

string hangman::getHint() {
	return this->hint;
}

string hangman::getGuessedWord() {
	return this->guessedWord;
}

int hangman::getNumOfGuesses(){
	return this->numOfGuesses;
}

int hangman::getNumOfFails() {
	return this->numOfFails;
}

int hangman::getRandomNum() {
	return this->randomNum;
}

void hangman::drawHangman(int numOfFails) {
	if (numOfFails == 9) {
		cout << endl << endl
			<< "   +----+     " << endl
			<< "   |    |     " << endl
			<< "   |    O     " << endl
			<< "   |   /|\\   " << endl
			<< "   |   / \\   " << endl
			<< "   |          " << endl
			<< "  =============" << endl;
	}
	else if (numOfFails == 8){
		cout << endl << endl
			<< "   +----+     " << endl
			<< "   |    |     " << endl
			<< "   |    O     " << endl
			<< "   |   /|\\   " << endl
			<< "   |     \\   " << endl
			<< "   |          " << endl
			<< "  =============" << endl << endl;
	}
	else if (numOfFails == 7){
		cout << endl << endl
			<< "   +----+     " << endl
			<< "   |    |     " << endl
			<< "   |    O     " << endl
			<< "   |   /|\\   " << endl
			<< "   |          " << endl
			<< "   |          " << endl
			<< "  =============" << endl << endl;
	}
	else if (numOfFails == 6) {
		cout << endl << endl
			<< "   +----+     " << endl
			<< "   |    |     " << endl
			<< "   |    O     " << endl
			<< "   |   /|     " << endl
			<< "   |          " << endl
			<< "   |          " << endl
			<< "  =============" << endl << endl;
	}
	else if (numOfFails == 5){
		cout << endl << endl
			<< "   +----+     " << endl
			<< "   |    |     " << endl
			<< "   |    O     " << endl
			<< "   |    |     " << endl
			<< "   |          " << endl
			<< "   |          " << endl
			<< "  =============" << endl << endl;
	}
	else if (numOfFails == 4){
		cout << endl << endl
			<< "   +----+     " << endl
			<< "   |    |     " << endl
			<< "   |    O     " << endl
			<< "   |          " << endl
			<< "   |          " << endl
			<< "   |          " << endl
			<< "  =============" << endl << endl;
	}
	else if (numOfFails == 3){
		cout << endl << endl
			<< "   +----+  " << endl
			<< "   |    |  " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "  =============" << endl << endl;
	}
	else if (numOfFails == 2){
		cout << endl << endl
			<< "   +----+  " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "  =============" << endl << endl;
	}
	else if (numOfFails == 1){
		cout << endl << endl
			<< "   +       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "   |       " << endl
			<< "  =============" << endl << endl;
	}
	else {
		cout << endl << endl
			<< "           " << endl
			<< "           " << endl
			<< "           " << endl
			<< "           " << endl
			<< "           " << endl
			<< "           " << endl
			<< "  =============" << endl << endl;
	}
}

void hangman::setGuessedWord() {
	for (unsigned i = 0; i < this->wordToGuess.size(); i++) {
		if (wordToGuess.at(i) == ' ') {
			this->guessedWord += "  ";
		}
		else {
			this->guessedWord += "_ ";
		}
	}
}

bool hangman::checkGuess(char guess) {

	bool b = false;
	for (unsigned i = 0; i < this->wordToGuess.size(); i++) {
		if (guess == this->wordToGuess.at(i)) {
			this->guessedWord.at(2 * i) = guess;
			b = true;
		}
	}

	return b;
}

void hangman::nogPlus() {
	this->numOfFails += 1;
}

bool hangman::checkWin() {
	bool b = true;

	for (unsigned i = 0; i < this->wordToGuess.size(); i++) {
		if (this->wordToGuess.at(i) != this->guessedWord.at(2 * i)) {
			b = false;
		}
	}
	return b;
}

void hangman::setToBegin() {
	this->numOfFails = 0;
	this->wordToGuess = this->loadWord();
	this->hint = this->loadHint();
	this->guessedWord = "";
	this->setGuessedWord();
}

void hangman::playTheGame() {
	while (true) {
		cout << "Welcome to Hangman game, good luck ;)" << endl;

		while (this->getNumOfFails() < this->getNumOfGuesses()) {
			cout << endl << "You have " << this->getNumOfGuesses() - this->getNumOfFails() << " more times to miss the guess." << endl;
			this->drawHangman(this->getNumOfFails());
			cout << this->getGuessedWord() << endl << endl;
			cout << "Here's a little hint for you. " <<
				endl << ">" << this->getHint() << "<" << endl << endl;
			char guess;
			cout << "Guess letter, type only one letter!" << endl;
			cout << ">";
			cin >> guess;
			cout << endl;
			cin.clear();
			cin.ignore();
			guess = toupper(guess);

			if (this->checkGuess(guess)) {
				if (this->checkWin()) {
					system("cls");
					this->drawHangman(this->getNumOfFails());
					cout << this->getGuessedWord() << endl << endl;
					cout << "Congratualtion! You guessed the word! :)" << endl;
					cout << "Word to guess was " << this->getWord() << endl;
					break;
				}
				this->drawHangman(this->getNumOfFails());
				cout << endl << "You found a letter!" << endl;

			}
			else {
				cout << endl << "Word doesn't contain letter " << guess << "." << endl;
				this->nogPlus();
			}
			system("cls");

		}
		if (this->getNumOfFails() == this->getNumOfGuesses()) {
			this->drawHangman(this->getNumOfGuesses());
			cout << endl << "Oops, you have been hanged :(" << endl;
			cout << "Word to guess was " << this->getWord() << endl;
		}

		char ag;
		cout << endl << "Do you wanna play again Y/N" << endl;
		cout << ">";
		cin >> ag;
		cout << endl;

		if (ag == 'y' || ag == 'Y') {
			this->setToBegin();
		}
		else {
			break;
		}
		system("cls");
	}

}