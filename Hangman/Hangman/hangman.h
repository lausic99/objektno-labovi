#pragma once
#include<iostream>
#include<string>
#include<fstream>
#include<vector>
#include <ctime>
#include <algorithm>
using namespace std;


class hangman {
private:
	string wordToGuess;
	string hint;
	string guessedWord;
	int numOfGuesses;
	int numOfFails;
	int randomNum;

public:
	hangman ();
	~hangman() {};
	string loadWord();
	string loadHint();
	string getWord();
	string getHint();
	string getGuessedWord();
	int getNumOfGuesses();
	int getNumOfFails();
	int getRandomNum();
	void drawHangman(int numOfFails);
	void setGuessedWord();
	bool checkGuess(char guess);
	void nogPlus();
	bool checkWin();
	void setToBegin();
	void playTheGame();


};
