#ifndef  LANDVEHICLE_H
#define LANDVEHICLE_H
#include"vehicle.h"
namespace oop {

	class landVehicle : virtual public vehicle {

	protected:
		string vehicleType;
		unsigned numOfPassengers;
	public:
		landVehicle() {};
		string type();

	};
}
#endif
