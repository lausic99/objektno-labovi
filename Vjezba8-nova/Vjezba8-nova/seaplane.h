#ifndef SEAPLANE_H
#define SEAPLANE_H

#include"watercraft.h"
#include"aircraft.h"
namespace oop {
	class seaplane : public aircraft, public watercraft {
	protected:
		unsigned numOfPassengers;
		string typeOfVehicle;
	public:
		seaplane(unsigned broj);
		unsigned passengers();
		string type();
	};
}
#endif