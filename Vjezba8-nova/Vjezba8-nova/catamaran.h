#ifndef CATAMARAN_H
#define CATAMARAN_H

#include "watercraft.h"
namespace oop {
	class catamaran : public watercraft {
	protected:
		unsigned numOfPassengers;
	public:
		catamaran(unsigned broj);
		unsigned passengers();

	};
}
#endif
