
#ifndef  VEHICLE_H
#define VEHICLE_H
#include<iostream>
#include <string>
using namespace std;

namespace oop {
	class vehicle {
	protected:

	public:
		virtual string type() = 0;
		virtual unsigned passengers() = 0;
		virtual ~vehicle() = 0;
	};
}


#endif 

