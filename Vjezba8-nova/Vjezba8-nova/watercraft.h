#ifndef  WATERCRAFT_H
#define WATERCRAFT_H
#include"vehicle.h"
namespace oop {
	class watercraft : virtual public oop::vehicle {

	protected:
		string vehicleType;
		unsigned numOfPassengers;
	public:
		watercraft() {};
		string type();

	};
}
#endif