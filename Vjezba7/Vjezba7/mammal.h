#include "zooanimal.h"


namespace OSS {
#ifndef  MAMMAL_H
#define MAMMAL_H

	class mammal : public ZooAnimal {

	protected:
		int gestationPeriod;
		double avgTemp;
		string wayOfReproduction;

	public:
		mammal(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int gestationPeriod, double avgTemp, string wayOfReproduction);
		friend ostream& operator <<(ostream& out, mammal& a);
		friend istream& operator >>(istream& in, mammal& a);
	};

#endif 

}