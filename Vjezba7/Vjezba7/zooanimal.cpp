#include "zooanimal.h"
using namespace std;

ZooAnimal::ZooAnimal(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek) {
	this->vrsta = vrsta;
	this->ime = ime;
	this->godina_rodenja = godina_rodenja;
	this->broj_kaveza = broj_kaveza;
	this->broj_dnev_obroka = broj_dnev_obroka;
	this->ocekivani_ziv_vijek = ocekivani_ziv_vijek;
	this->masa = new Masa[this->ocekivani_ziv_vijek * 2];
	//cout << "Poziv konstruktora" << endl;
}
ZooAnimal::ZooAnimal(const ZooAnimal& other) {
	vrsta = other.vrsta;
	ime = other.ime;
	godina_rodenja = other.godina_rodenja;
	broj_kaveza = other.broj_kaveza;
	broj_dnev_obroka = other.broj_dnev_obroka;
	ocekivani_ziv_vijek = other.ocekivani_ziv_vijek;
	masa = new Masa[2 * ocekivani_ziv_vijek];
	for (int i = 0; i < 2 * ocekivani_ziv_vijek; i++) {
		masa[i] = other.masa[i];
	}
}
ZooAnimal::~ZooAnimal() {
	delete[] masa;
	//cout << "Poziv destruktora" << endl;
}

void ZooAnimal::obroci() {
	if (this->zdebljalasise()) {
		this->broj_dnev_obroka += 1;
	}
	else if (!this->zdebljalasise()) {
		this->broj_dnev_obroka -= 1;
	}
}
int Masa::get_godinu() const {
	return this->godina;
}
double Masa::get_masu() const {
	return this->masa;
}
void Masa::set_masu(Masa* tmp) {
	this->godina = tmp->get_godinu();
	this->masa = tmp->get_masu();
}

void ZooAnimal::dod_pod_masi(Masa* tmp) {
	for (int i = 0; i < (this->ocekivani_ziv_vijek) * 2; i++) {
		if (tmp->get_godinu() == masa[i].get_godinu() && masa[i].get_masu() != 0) {
			return;
		}
	}
	for (int i = 0; i < (this->ocekivani_ziv_vijek) * 2; i++) {
		if (masa[i].get_masu() == 0) {
			masa[i].set_masu(tmp);
			return;
		}
	}
}

int ZooAnimal::zdebljalasise() const {
	for (int i = 0; i < this->ocekivani_ziv_vijek * 2; i++) {
		if (this->masa[i].get_godinu() == now->tm_year + 1900 && this->masa[i].get_masu() != 0) {
			double x = masa[i].get_masu();
			for (int j = 0; j < this->ocekivani_ziv_vijek * 2 && j != i; j++) {
				if (this->masa[j].get_godinu() == now->tm_year + 1900 - 1) {
					double y = this->masa[j].get_masu();
					if (x / y > 1) {
						cout << "Zivotinja se zdebljala" << endl;
						return 0;

					}
					else if (x / y < 0.1) {
						cout << "Zivotinja je smrsavila" << endl;
						return 1;
					}
				}
			}
		}
	}

}


void ZooAnimal::print() const {
	cout << "Vrsta: " << this->vrsta << endl;
	cout << "Ime: " << this->ime << endl;
	cout << "Godina rodenja: " << this->godina_rodenja << endl;
	cout << "Broj kaveza: " << this->broj_kaveza << endl;
	cout << "Broj dnevnih obroka: " << this->broj_dnev_obroka << endl;
	cout << "Ocekivani zivotni vijek: " << this->ocekivani_ziv_vijek << endl;
	for (int i = 0; i < this->ocekivani_ziv_vijek * 2; i++) {
		cout << "Godina:" << this->masa[i].get_godinu() << endl;
		cout << "Masa: " << this->masa[i].get_masu() << endl;
	}
	cout << endl;
}

ostream& operator <<(ostream& out, ZooAnimal& a) {
	cout << "Vrsta: " << a.vrsta << endl;
	cout << "Ime: " << a.ime << endl;
	cout << "Godina rodenja: " << a.godina_rodenja << endl;
	cout << "Broj kaveza: " << a.broj_kaveza << endl;
	cout << "Broj dnevnih obroka: " << a.broj_dnev_obroka << endl;
	cout << "Ocekivani zivotni vijek: " << a.ocekivani_ziv_vijek << endl;
	for (int i = 0; i < a.ocekivani_ziv_vijek * 2; i++) {
		cout << "Godina:" << a.masa[i].get_godinu() << endl;
		cout << "Masa: " << a.masa[i].get_masu() << endl;
	}
	cout << endl;

	return out;
}