#include"reptile.h"
namespace OSS {
#ifndef TURTLE_H
#define TURTLE_H

	class turtle : public reptile {

	public:

		turtle(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int timeOfIncubaiton, double avgTemp, string wayOfReproduction);
		double foodQuantity;
		void get_foodQuantity() const;
		friend ostream& operator <<(ostream& out, OSS::turtle& a);
	};
#endif
}