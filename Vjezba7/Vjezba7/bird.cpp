#include "bird.h"

OSS::bird::bird(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int  timeOfIncubaiton, double avgTemp, string wayOfReproduction)
	:ZooAnimal(vrsta, ime, godina_rodenja, broj_kaveza, broj_dnev_obroka, ocekivani_ziv_vijek) {
	this -> timeOfIncubaiton = timeOfIncubaiton;
	this-> avgTemp = avgTemp;
	this-> wayOfReproduction = wayOfReproduction;
}

ostream& OSS::operator <<(ostream& out, OSS::bird&a) {
	out << "Vrsta: " << a.vrsta << endl;
	out << "Ime: " << a.ime << endl;
	out << "Godina rodenja: " << a.godina_rodenja << endl;
	out << "Broj kaveza: " << a.broj_kaveza << endl;
	out << "Broj dnevnih obroka: " << a.broj_dnev_obroka << endl;
	out << "Ocekivani zivotni vijek: " << a.ocekivani_ziv_vijek << endl;
	for (int i = 0; i < a.ocekivani_ziv_vijek * 2; i++) {
		out << "Godina:" << a.masa[i].get_godinu() << endl;
		out << "Masa: " << a.masa[i].get_masu() << endl;
	}
	out << "Vrijeme inkubacije: " << a.timeOfIncubaiton << endl;
	out << "Prosjecna temp.: " << a.avgTemp << endl;
	out << "Nacin razmnozavanja: " << a.wayOfReproduction << endl;

	return out;
}


istream& OSS::operator >>(istream& in, OSS::bird&a) {
	in >> a.timeOfIncubaiton >> a.avgTemp >> a.wayOfReproduction;
	return in;
}

