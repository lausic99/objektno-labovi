#include "crocodile.h"

OSS::crocodile::crocodile(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int timeOfIncubaiton, double avgTemp, string wayOfReproduction)
	: reptile(vrsta, ime, godina_rodenja, broj_kaveza, broj_dnev_obroka, ocekivani_ziv_vijek, timeOfIncubaiton, avgTemp, wayOfReproduction) {
	foodQuantity = 3;
}
void OSS::crocodile::get_foodQuantity() const {
	cout << "Ukupna kolicina hrane: " << this->foodQuantity << " kg" << endl;
}

ostream& OSS::operator <<(ostream& out, OSS::crocodile& a) {
	out << "Nacin razmnozavanja: " << a.wayOfReproduction << endl;
	out << "Prosjecna temp.: " << a.avgTemp << endl;
	out << "Gestacijski period: " << a.timeOfIncubation << endl;
	out << "Ukupna kolicina hrane: " << a.foodQuantity << endl;

	return out;
}