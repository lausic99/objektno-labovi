#include"bird.h"
namespace OSS {
#ifndef GRIFFONVULTURE_H
#define GRIFFONVULTURE_H

	class griffonVulture : public bird {

	public:

		griffonVulture(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int timeOfIncubaiton, double avgTemp, string wayOfReproduction);
		double foodQuantity;
		void get_foodQuantity() const;
		friend ostream& operator <<(ostream& out, OSS::griffonVulture& a);

	};
#endif
}