#include"mammal.h"
namespace OSS {
#ifndef TIGER_H
#define TIGER_H

	class tiger : public mammal {

	public:

		tiger(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int gestationPeriod, double avgTemp, string wayOfReproduction);
		double foodQuantity;
		void get_foodQuantity() const;
		friend ostream& operator <<(ostream& out, OSS::tiger& a);
	};
#endif
}