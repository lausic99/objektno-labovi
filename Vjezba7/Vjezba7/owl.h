#include"bird.h"
namespace OSS {
#ifndef OWL_H
#define OWL_H

	class owl : public bird {

	public:

		owl(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int timeOfIncubaiton, double avgTemp, string wayOfReproduction);
		double foodQuantity;
		void get_foodQuantity() const;
		friend ostream& operator <<(ostream& out, OSS::owl& a);

	};
#endif
}