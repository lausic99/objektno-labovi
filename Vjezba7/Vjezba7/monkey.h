#include"mammal.h"
namespace OSS {
#ifndef MONKEY_H
#define MONKEY_H

	class monkey : public mammal {

	public:

		monkey(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int gestationPeriod, double avgTemp, string wayOfReproduction);
		double foodQuantity;
		void get_foodQuantity() const;
		friend ostream& operator <<(ostream& out, OSS::monkey& a);

	};
#endif
}