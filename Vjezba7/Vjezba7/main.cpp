﻿#include <iostream>
#include <vector>

#include "tiger.h"
#include "monkey.h"
#include "elephant.h"
#include "griffonVulture.h"
#include "owl.h"
#include "crocodile.h"
#include "turtle.h"


void main() {
	OSS::tiger Tigraac("Tigar", "Pero", 2000, 5, 95, 5, 3, 33.5, "Radanje");
	//cout << Tigraac;
	//cin >> Tigraac;
	//cout << Tigraac;
	//Tigraac.get_foodQuantity();

	OSS::monkey Majmunče("Majmun", "Esad", 2017, 4, 3, 9, 7, 32.4, "Radanje");
	//cout << Majmunče;

	OSS::elephant Slon("Slon", "Mrki", 1990, 9, 6, 10, 8, 45.2, "Radanje");
	//cout << Slon;

	OSS::griffonVulture BjeloglaviSup("Bjeloglavi sup", "Egg", 2003, 7, 3, 5, 4, 12.9, "Polaganje jaja");
	//cout << BjeloglaviSup;
	//cin >> BjeloglaviSup;
	//cout << BjeloglaviSup;

	OSS::owl Sova("Sova", "ćuk", 2008, 33, 4, 6, 5, 22.8, "Polaganje jaja");
	//cout << Sova;
	//Sova.get_foodQuantity();


	OSS::crocodile Krokodil("Krokodil", "Sime", 1974, 2, 8, 20, 12, 11.7, "Polaganje jaja");
	//cout << Krokodil;
	//cin >> Krokodil;
	//cout << Krokodil;

	OSS::turtle Kornjaca("Kornjaca", "Speedi", 1800, 11, 4, 20, 13, 20.4, "Polaganje jaja");
	//cout << Kornjaca;


	vector <ZooAnimal> vektor;
	vektor.push_back(Tigraac);
	cout << vektor[0];
	cout << Tigraac;

	vektor.push_back(Majmunče);
	cout << vektor[1];
	cout << Majmunče;

	vektor.push_back(Slon);
	cout << vektor[2];
	cout << Slon;

	vektor.push_back(BjeloglaviSup);
	cout << vektor[3];
	cout << BjeloglaviSup;

	vektor.push_back(Sova);
	cout << vektor[4];
	cout << Sova;

	vektor.push_back(Krokodil);
	cout << vektor[5];
	cout << Krokodil;


	vektor.push_back(Kornjaca);
	cout << vektor[6];
	cout << Kornjaca;



}