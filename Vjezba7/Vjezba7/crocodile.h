#include"reptile.h"
namespace OSS {
#ifndef CROCODILE_H
#define CROCODILE_H

	class crocodile : public reptile {

	public:

		crocodile(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int timeOfIncubaiton, double avgTemp, string wayOfReproduction);
		double foodQuantity;
		void get_foodQuantity() const;
		friend ostream& operator <<(ostream& out, OSS::crocodile& a);

	};
#endif
}
