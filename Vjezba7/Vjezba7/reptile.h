#include "zooanimal.h"
namespace OSS {
#ifndef REPTILE_H
#define REPTILE_H

	class reptile : public ZooAnimal {

	protected:
		int timeOfIncubation;
		double avgTemp;
		string wayOfReproduction;

	public:
		reptile(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int timeOfIncubaiton, double avgTemp, string wayOfReproduction);
		friend ostream& operator <<(ostream& out, reptile& a);
		friend istream& operator >>(istream& in, reptile& a);
	};

#endif 
}