#include"monkey.h"

OSS::monkey::monkey(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int gestationPeriod, double avgTemp, string wayOfReproduction)
	: mammal(vrsta, ime, godina_rodenja, broj_kaveza, broj_dnev_obroka, ocekivani_ziv_vijek, gestationPeriod, avgTemp, wayOfReproduction) {
	foodQuantity = 7;
}
void OSS::monkey::get_foodQuantity() const {
	cout << "Ukupna kolicina hrane: " << this->foodQuantity << " kg" << endl;
}

ostream& OSS::operator <<(ostream& out, OSS::monkey& a) {
	out << "Nacin razmnozavanja: " << a.wayOfReproduction << endl;
	out << "Prosjecna temp.: " << a.avgTemp << endl;
	out << "Gestacijski period: " << a.gestationPeriod << endl;
	out << "Ukupna kolicina hrane: " << a.foodQuantity << endl;

	return out;
}