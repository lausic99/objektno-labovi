#include "zooanimal.h"
namespace OSS {
#ifndef  BIRD_H
#define BIRD_H

	class bird : public ZooAnimal {

	protected:
		int timeOfIncubaiton;
		double avgTemp;
		string wayOfReproduction;

	public:
		bird(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int timeOfIncubaiton, double avgTemp, string wayOfReproduction);
		friend ostream& operator <<(ostream& out, bird& a);
		friend istream& operator >>(istream& in, bird& a);
	};

#endif 
}
