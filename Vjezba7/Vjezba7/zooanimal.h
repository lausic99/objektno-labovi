﻿#define _CRT_SECURE_NO_WARNINGS
#ifndef ZOOANIMAL_H
#define ZOOANIMAL_H
#include<iostream>
#include<string>
#include <ctime>
#include<string>

using namespace std;

static time_t t = time(0);
static struct tm* now = localtime(&t);


class Masa {
	double masa;
	int godina;
public:
	Masa() {
		godina = now->tm_year + 1900;
		masa = 0;
	}
	Masa(int year, double masa) {
		this->godina = year;
		this->masa = masa;
	}
	int get_godinu()const;
	double get_masu()const;
	void set_masu(Masa* tmp);


};

class ZooAnimal {
public:
	int broj_dnev_obroka;
protected:
	string vrsta;
	string ime;
	int godina_rodenja;
	int broj_kaveza;
	int ocekivani_ziv_vijek;
	Masa* masa;
public:

	ZooAnimal(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek);
	ZooAnimal(const ZooAnimal& other);
	~ZooAnimal();
	void obroci();//funkciju za promjenu broja obroka(smanjenje ili uve´canje za 1)
	void dod_pod_masi(Masa* tmp);//funkciju koja dodaje podatke o masi za odredenu godinu
	int zdebljalasise()const;//funkciju koja detektira da li se ˇzivotinja udebljala ili je smrˇsavila
	void print()const;//funkciju koja ispisuje podatke o objektu
	friend ostream& operator <<(ostream& out, ZooAnimal& a);

};
#endif
