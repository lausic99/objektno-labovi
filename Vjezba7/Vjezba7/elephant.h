#include"mammal.h"
namespace OSS {
#ifndef ELEPHANT_H
#define ELEPHANT_H

	class elephant : public mammal {

	public:

		elephant(string vrsta, string ime, int godina_rodenja, int broj_kaveza, int broj_dnev_obroka, int ocekivani_ziv_vijek, int gestationPeriod, double avgTemp, string wayOfReproduction);
		double foodQuantity;
		void get_foodQuantity() const;
		friend ostream& operator <<(ostream& out, OSS::elephant& a);

	};
#endif
}