#ifndef ORUZJE_H
#define ORUZJE_H

#include"../Zadatak1/point.h"
class oruzje {
	Pozicija pozicija;
	int broj_metaka;
	int trenutni_broj_metaka;

public:
	void postavi(double d, double s, double v);
	void post_br_met(int n);
	void shoot();
	void reload();
	double get_x()const;
	double get_y()const;
	double get_z()const;
};

#endif // !ORUZJE_H
