/*Napisati klasu koja predstavlja oruzje. Oruzje ima svoj polozaj u prostoru
(jedna tocka u prostoru), broj metaka koji stanu u jedno punjenje i trenutni broj metaka u punjenju.
Moze pucati (shoot) i ponovo se puniti (reload).*/
#include "oruzje.h"
#include <iostream>
using namespace std;

int main() {
	oruzje pistolj;
	pistolj.postavi(2, 2.35, 2);
	cout << pistolj.get_y();
}