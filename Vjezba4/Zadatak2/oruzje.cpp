#include"oruzje.h"
using namespace std;
void oruzje::postavi(double d, double s, double v) {
	this->pozicija.postavi(d, s, v);

}
void oruzje::post_br_met(int n) {
	this->broj_metaka = n;
}

void oruzje::shoot() {
	this->trenutni_broj_metaka -= 1;
}
void oruzje::reload() {
	this->trenutni_broj_metaka = this->broj_metaka;
}

double oruzje::get_x() const {
	return this->pozicija.get_x();
}
double oruzje::get_y() const {
	return this->pozicija.get_y();
}
double oruzje::get_z() const {
	return this->pozicija.get_z();
}