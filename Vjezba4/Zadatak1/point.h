#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

class Pozicija {
	double duzina;
	double sirina;
	double visina;

public:
	void postavi(double d = 0.0, double s = 0.0, double v = 0.0);
	void rand_postavi(int a, int b);
	double get_x()const;
	double get_y()const;
	double get_z()const;
	double distanca_2d(Pozicija k);
	double distanca_3d(Pozicija k);
	void print()const;
};
#endif 
