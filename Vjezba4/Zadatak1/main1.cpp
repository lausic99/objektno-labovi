﻿/*Napisati klasu za poziciju toˇcke u prostoru koja ima tri double podatka : duzinu, sirinu
i visinu.Klasa ima sljede´ce funkcije :
	• Funkciju za postavljanje vrijednosti clanova(treba imati pretpostavljene vrijednosti 0).
	• Funkciju za postavljanje pseudorandom vrijednosti clanova(granice intervala su
		parametri funkcije).
	• Funkciju za dohvacanje vrijednosti clanova.
	• Funkciju koja racuna udaljenost do druge pozicije u 2D ravnini.
	• Funkciju koja racuna udaljenost do druge pozicije u 3D prostoru.
U main funkciji postavite vrijednost za dvije tocke u prostoru i izracunajte obe udaljenosti.*/

#include <iostream>
#include<cstdlib>
#include <time.h>
#include "point.h"

int main() {
	srand(time(0));
	Pozicija p;
	Pozicija k;
	p.postavi(26, 70, 30);
	k.postavi(30, 60, 50);
	p.print();
	k.print();
	cout << p.distanca_2d(k) << endl;
	cout << p.distanca_3d(k) << endl;

}