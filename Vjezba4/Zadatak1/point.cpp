#include "point.h"
#include <time.h>


void Pozicija::rand_postavi(int a, int b) {
	if (a > b) {
		int tmp = a;
		a = b;
		b = tmp;
	}
	this->duzina = rand() % (b - a) + a;
	this->sirina = rand() % (b - a) + a;
	this->visina = rand() % (b - a) + a;
}

void Pozicija::postavi(double d, double s, double v) {
	this->duzina = d;
	this->sirina = s;
	this->visina = v;
}



double Pozicija::get_x()const {
	return duzina;
}
double Pozicija::get_y() const {
	return sirina;
}
double Pozicija::get_z() const {
	return visina;
}

double Pozicija::distanca_2d(Pozicija k) {
	return sqrt((k.get_x() - this->get_x())*(k.get_x() - this->get_x()) + (k.get_y() - this->get_y())*(k.get_y() - this->get_y()));
}
double Pozicija::distanca_3d(Pozicija k) {
	return sqrt((k.get_x() - this->get_x()) * (k.get_x() - this->get_x()) + (k.get_y() - this->get_y()) * (k.get_y() - this->get_y()) + (k.get_z() - this->get_z())*(k.get_z() - this->get_z()));
}

void Pozicija::print()const {
	cout << this->duzina << "  " << this->sirina << "  " << this->visina << endl;
}