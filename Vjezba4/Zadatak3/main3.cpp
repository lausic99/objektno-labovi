﻿/*Napisati klasu koja predstavlja metu. Meta je oblika pravokutnika i zadana je donjom
lijevom toˇckom, ˇsirinom i visinom. Ima dva stanja: pogodena i nepogodena.*/

#include "meta.h"


int main() {
	meta M;
	M.postavi(3.1, 2.5, 8.8);
	cout << M.get_x();
}