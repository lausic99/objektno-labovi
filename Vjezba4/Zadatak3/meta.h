#ifndef META_H
#define META_H

#include <iostream>
#include"../Zadatak2/oruzje.h"
#include "../Zadatak1/point.h"
using namespace std;

class meta {
	Pozicija pozicija;
	double sirina;
	double visina;
	bool pogodena;
public:
	void postavi_stanje(const int pogodak);

	void postavi(const double x = 0, const double y = 0, const double z = 0);
	void postavi_sirinu(const double x);
	void postavi_visinu(const double x);
	int get_stanje()const;
	double get_x()const;
	double get_y()const;
	double get_z()const;
	double get_sirinu()const;
	double get_visinu()const;


};

#endif 
