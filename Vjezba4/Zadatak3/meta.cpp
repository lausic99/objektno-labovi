#include "meta.h"

void meta::postavi_stanje(const int  pogodak) {
	this->pogodena = pogodak;
}

void meta::postavi(const double x, const double y, const double z) {
	this->pozicija.postavi(x, y, z);
}

void meta::postavi_sirinu(const double x) {
	this->sirina = x;
}

void meta::postavi_visinu(const double x) {
	this->visina = x;
}

int meta::get_stanje() const {
	return this->pogodena;
}

double meta::get_x()const {
	return this->pozicija.get_x();
}
double meta::get_y()const {
	return this->pozicija.get_y();
}
double meta::get_z()const {
	return this->pozicija.get_z();
}
double meta::get_sirinu()const {
	return this->sirina;
}
double meta::get_visinu()const {
	return this->visina;
}