﻿/*Napisati program koji ´ce generirati n meta i jednog pucaˇca (oruˇzje).
Uz pretpostavku da je poloˇzaj oruˇzja horizontalan i da se oruˇzje moˇze
zakretati za 360◦izraˇcunati koliko je meta pogodeno jednim punjenjem.*/

#include <iostream>
#include"../Zadatak1/point.h"
#include "../Zadatak2/oruzje.h"
#include "../Zadatak3/meta.h"
using namespace std;

int main() {
	int n_meta;
	cout << "Unesite koliko meta: ";
	cin >> n_meta;
	cout << endl;

	int n_metaka;
	cout << "Unesite koliko metaka po magazinu: ";
	cin >> n_metaka;
	cout << endl;

	oruzje pistolj;
	double x_pistolj;
	double y_pistolj;
	double z_pistolj;
	cout << "Unesite koordinate pistolja: ";
	cin >> x_pistolj >> y_pistolj >> z_pistolj;
	pistolj.postavi(x_pistolj, y_pistolj, z_pistolj);
	pistolj.post_br_met(n_metaka);

	meta* mete = new meta[n_meta];
	for (int i = 0; i < n_meta; i++) {
		cout << "Unesite koordinate " << i << ". mete:";
		double x_meta;
		double y_meta;
		double z_meta;
		cin >> x_meta >> y_meta >> z_meta;
		mete[i].postavi(x_meta, y_meta, z_meta);
		cout << "Unesite sirinu i visinu " << i << ". mete:";
		double sirina_meta;
		double visina_meta;
		cin >> sirina_meta >> visina_meta;
		mete[i].postavi_sirinu(sirina_meta);
		mete[i].postavi_visinu(visina_meta);
		mete[i].postavi_stanje(0);
	}

	for (int i = 0; i < n_meta; i++) {
		cout << endl << mete[i].get_sirinu();
		cout << " " << mete[i].get_visinu();
	}


}