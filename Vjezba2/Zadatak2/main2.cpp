﻿/*Napiˇsite funkciju koja odvaja parne i neparne brojeve u nizu
tako da se prvo pojavljuju parni brojevi, a zatim neparni brojevi.
Memoriju za niz alocirati dinamiˇcki. Primjer ulaza i izlaza:
ulaz: 7 2 4 9 10 11 13 27
izlaz: 10 2 4 9 7 11 13 27*/

#include <iostream>
using namespace std;

int* razdvoji(int* niz, int len) {

	int* noviniz;
	noviniz = new int[len];
	int p = 0, k = 0;
	for (int i = 0; i < len; i++) {
		if (niz[i] % 2 == 0) {
			noviniz[p] = niz[i];
			p++;
		}
		else {
			noviniz[len - k - 1] = niz[i];
			k++;
		}
	}
	delete[] niz;
	niz = 0;

	return noviniz;
}

int main() {
	cout << "koliko clanova: ";
	int len;
	cin >> len;
	cout << endl;

	int* niz;
	niz = new int[len];
	for (int i = 0; i < len; i++) {
		cout << "Unesite broj: ";
		cin >> niz[i];
		cout << endl;
	}

	niz = razdvoji(niz, len);

	for (int i = 0; i < len; i++) {
		cout << niz[i] << " ";
	}

	delete[] niz;
	niz = 0;
}
