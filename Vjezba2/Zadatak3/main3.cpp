﻿/*Deﬁnirati strukturu koja simulira rad vektora.
Struktura se sastoji od niza int elemenata, logiˇcke i ﬁziˇcke veliˇcine niza.
Fiziˇcka veliˇcina je inicijalno init, a kada se ta veliˇcina napuni vrijednostima, alocira se duplo.
Napisati implementacije za funkcije
vector new, ++
vector delete, ++
vector push back - inserts to the last position ++
vector pop back - removes last element  ++
vector front - Returns a reference to the first element in the vector++
vector back - Returns a reference to the last element in the vector++
vector size - Returns the number of elements in the vector ++
*/

#include <iostream>
using namespace std;

typedef struct vec {
	int* niz;
	int log_len;
	int fiz_len;
}vek;

vek* new_vek() {
	vek* novi;
	novi = new vek;
	cout << "Unesite broj elemenata: ";
	cin >> novi->log_len;
	novi->niz = new int[novi->log_len];
	novi->fiz_len = 0;
	return novi;
}
void del_vek(vek* vektor) {
	delete[] vektor->niz;
	vektor->niz = 0;
	vektor->fiz_len = -1;
	vektor->log_len = -1;
	delete vektor;
}

void push_back(vek* vektor, int broj) {
	vektor->niz[vektor->fiz_len] = broj;
	vektor->fiz_len += 1;
}

int pop_back(vek*vektor) {
	vektor->fiz_len -= 1;
	return vektor->niz[vektor->fiz_len];
}

int* front_vek(vek* vektor) {
	return &vektor->niz[0];
}

int* back_vek(vek* vektor) {
	return &vektor->niz[vektor->fiz_len - 1];
}

int size_vek(vek* vektor) {
	return vektor->fiz_len;
}

int main() {
	/*vek* vektor = new_vek();
	for (int i = 0; i < vektor->log_len; i++) {
		cout << "Unesite broj: ";
		int broj;
		cin >> broj;
		cout << endl;
		push_back(vektor, broj);
	}
	for (int i = 0; i < vektor->log_len; i++) {
		cout << pop_back(vektor) <<" ";
	}
	//cout << *front_vek(vektor) << "   " << *back_vek(vektor);
	cout << size_vek(vektor);
	pop_back(vektor);
	pop_back(vektor);
	cout << size_vek(vektor);
	del_vek(vektor);
	cout << size_vek(vektor);
	*/

	int odabir = -1;
	vek* vektor = new_vek();
	while (odabir != 2) {
		if (vektor->fiz_len == vektor->log_len) {
			vektor->log_len *= 2;
			int *niz = new int[vektor->log_len];
			for (int i = 0; i < vektor->fiz_len; i++) {
				niz[i] = vektor->niz[i];
			}
			delete[] vektor->niz;
			vektor->niz = niz;
		}
		cout << "Odaberite: " << endl;
		cout << "1-Vector size" << endl;
		cout << "2-Izbrisi vektor" << endl;
		cout << "3-Push back" << endl;
		cout << "4-Pop back" << endl;
		cout << "5-Vector front" << endl;
		cout << "6-Vector back" << endl;
		cin >> odabir;

		switch (odabir) {
		case 1:
			cout << "Vector size:" << size_vek(vektor) << endl;
			break;
		case 2:
			del_vek(vektor);
			break;
		case 3:
			cout << "Unesite broj: ";
			int broj;
			cin >> broj;
			cout << endl;
			push_back(vektor, broj);
			break;
		case 4:
			cout << pop_back(vektor) << endl;
			break;
		case 5:
			cout << *front_vek(vektor) << endl;
			break;
		case 6:
			cout << *back_vek(vektor) << endl;
			break;
		default:
			break;
		}
	}
}
